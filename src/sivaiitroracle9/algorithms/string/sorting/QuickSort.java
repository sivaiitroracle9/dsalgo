package sivaiitroracle9.algorithms.string.sorting;

public class QuickSort {

	public static void main(String[] args) {
		int[] array = SortUtils.randomArray(10);
		SortUtils.printArray(array);
		array = quickSort(array, 0, array.length - 1);
		SortUtils.printArray(array);
	}

	private static int[] quickSort(int[] array, int left, int right) {

		if (left >= right)
			return array;

		// Partition and Placing the pivot in correct place.
		int pivot = array[right];
		int finalPivotIndex = left;
		for (int i = left; i < right; i++) {
			if (array[i] <= pivot) {
				array = SortUtils.swap(array, finalPivotIndex, i);
				finalPivotIndex++;
			}
		}
		array = SortUtils.swap(array, finalPivotIndex, right);

		// Branch the array.
		array = quickSort(array, left, finalPivotIndex - 1);
		array = quickSort(array, finalPivotIndex + 1, right);

		return array;
	}
}
