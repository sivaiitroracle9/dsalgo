package sivaiitroracle9.algorithms.string.sorting;

public class MaxHeapWithArray {
	
	public static void main(String[] args) {
		int[] array = SortUtils.randomArray(10);
		SortUtils.printArray(array);
		array = maxHeapify(array);
		SortUtils.printArray(array);
	}
	
	private static int[] maxHeapify(int[] array) {
		
		for (int i=array.length/2 -1; i>=0; i--) {
			array = maxHeapify(array, i);
		}
		return array;
	}

	private static int[] maxHeapify(int[] array, int n) {

		if (array.length-1 >= 2 * n + 2) {

			if ((array[2 * n + 1] > array[2 * n + 2]) && (array[2 * n + 1] > array[n])) {
				array = SortUtils.swap(array, n, 2 * n + 1);
				//percolation
				array = maxHeapify(array, 2*n + 1);
			}

			if ((array[2 * n + 1] < array[2 * n + 2])
					&& (array[2 * n + 2] > array[n])) {
				array = SortUtils.swap(array, n, 2 * n + 2);
				//percolation
				array = maxHeapify(array, 2*n + 2);
			}

		} else if (array.length -1 >= 2 * n + 1) {
			if (array[2 * n + 1] > array[n]) {
				array = SortUtils.swap(array, n, 2 * n + 1);
				//percolation
				array = maxHeapify(array, 2*n + 1);
			}
		}
		
		return array;
	}

}
