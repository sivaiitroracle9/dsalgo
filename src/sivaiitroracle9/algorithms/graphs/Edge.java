package sivaiitroracle9.algorithms.graphs;

public class Edge {
	public int v;
	public int w;
	
	public double weight;
	
	public Edge(int v, int w, double weight) {
		this.v = v;
		this.w = w;
		this.weight = weight;
	}
	
	public boolean equals(Edge that){
		if (this.v == that.v && this.w == that.w) {
			return true;
		}
		return false;
	}
}
