package sivaiitroracle9.designpatterns.java;

public class SingletonDesign {

	// very critical to make INSTANCE volatile.
	// not keeping it volatile may affect singleton in following way
	// Suppose one thread goes to line INSTANCE = new Connection().
	// Now since INSTANCE is not volatile this initialization may not be
	// seen immediately in other thread and that thread may still
	// think INSTANCE is null and will try to initialize it
	private static volatile SingletonDesign INSTANCE = null;

	// SingletonDesign class should not be instantiated from outside.
	private SingletonDesign() {

	}

	public static SingletonDesign getInstance() {

		if (INSTANCE == null) { // for this if check to be consistent, INSTANCE
								// should be 'volatile'.
			// This makes the instantiation thread safe.
			synchronized (SingletonDesign.class) {
				if (INSTANCE == null) {
					INSTANCE = new SingletonDesign();
				}
			}
		}

		return INSTANCE;
	}

}
