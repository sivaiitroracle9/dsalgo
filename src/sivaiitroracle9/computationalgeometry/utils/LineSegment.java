package sivaiitroracle9.computationalgeometry.utils;

public class LineSegment {

	Point left;
	Point right;

	public LineSegment(Point p1, Point p2) {
		if (p1.X() < p2.X()) {
			this.left = p1;
			this.right = p2;
		} else if (p1.X() > p2.X()) {
			this.left = p2;
			this.right = p1;
		} else if (p1.X() == p2.X()) {
			if (p1.Y() > p2.Y()) {
				this.left = p2;
				this.right = p1;
			} else {
				this.left = p1;
				this.right = p2;
			}
		}

	}

	public DIRECTION positionOfPoint(Point p) {

		Point d1 = this.left.differnce(right);
		Point d2 = this.left.differnce(p);

		double dir = Math.signum(d1.X() * d2.Y() - d2.X() * d1.Y());

		if (dir == 1) {
			return DIRECTION.ANTICLOCKWISE;
		} else if (dir == -1) {
			return DIRECTION.CLOCKWISE;
		} else {
			return DIRECTION.LINEAR;
		}

	}

	public enum DIRECTION {
		ANTICLOCKWISE, CLOCKWISE, LINEAR;
	}

	public double heightFromLine(Point point) {
		double lp = this.left.distance(point);
		double rp = this.right.distance(point);
		double lr = this.left.distance(right);

		double height = Math.sqrt(Math.pow(rp, 2) - 0.25
				* (1 + (Math.pow(rp, 2) - Math.pow(lp, 2)) / Math.pow(lr, 2)));

		return height;
	}
}
