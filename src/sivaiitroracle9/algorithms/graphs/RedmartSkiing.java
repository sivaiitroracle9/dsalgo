package sivaiitroracle9.algorithms.graphs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

public class RedmartSkiing {

	private static int rows = 0;
	private static int cols = 0;
	private static int[][] matrix = null;

	public static void main(String[] args) {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		try {

			String text = br.readLine();
			String[] dimensions = text.split(" ");

			if (dimensions.length == 2) {
				rows = Integer.parseInt(dimensions[0].trim());
				cols = Integer.parseInt(dimensions[1].trim());
			} else {
				System.out.println("IO error trying to read your name!");
				System.exit(1);
			}

			matrix = new int[rows][cols];
			for (int x = 0; x < rows; x++) {
				text = br.readLine();
				text = text.trim();
				String[] rowdata = text.split(" ");
				for (int y = 0; y < cols; y++) {
					matrix[x][y] = Integer.parseInt(rowdata[y].trim());
				}
			}

		} catch (IOException e) {
			System.out.println("Error reading Input!! IO Exception.");
		} catch (Exception e) {
			System.out.println("Error reading Input!!");
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out
				.println("-----------------------------INPUT READ-----------------------");
		TreeMap<Integer, Map<Cell, List<Cell>>> depthMap = findLongestDepth(matrix);
		NavigableMap<Integer, Map<Cell, List<Cell>>> reverseDepthMap = depthMap
				.descendingMap();

		for (Cell cell : (depthMap.get(reverseDepthMap.firstKey())).keySet()) {
			List<Cell> path = (depthMap.get(reverseDepthMap.firstKey()))
					.get(cell);
			if (path == null) {
				System.out.println("No Path");
			} else {
				String line = "";
				String valline = "";
				int dp = 0;
				for (Cell c : path) {
					line += c.toString() + "->";
					valline += matrix[c.x][c.y] + "->";
					dp = matrix[c.x][c.y];
				}
				System.out.println(line + (matrix[cell.x][cell.y] - dp));
				System.out.println("|||||{" + valline + "}|||||");
				System.out.println();
			}
		}

	}

	private static TreeMap<Integer, Map<Cell, List<Cell>>> findLongestDepth(
			int[][] matrix) {

		TreeMap<Integer, Map<Cell, List<Cell>>> depthMap = new TreeMap<Integer, Map<Cell, List<Cell>>>();

		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {
				Cell curCell = new Cell(i, j);

				List<Cell> maxDepthPath = findLongestDepth(matrix,
						Integer.MAX_VALUE, curCell,
						new boolean[matrix.length][matrix[0].length]);
				if (maxDepthPath != null) {
					int depth = maxDepthPath.size();
					if (depthMap.get(depth) == null) {
						depthMap.put(depth, new HashMap<Cell, List<Cell>>());
					}

					depthMap.get(depth).put(curCell, maxDepthPath);

				}
				System.out.println(curCell.toString());
			}
		}

		return depthMap;
	}

	private static List<Cell> findLongestDepth(int[][] matrix,
			int prevCellValue, Cell start, boolean[][] visitedCells) {

		if (!isCellValid(start, matrix) || visitedCells[start.x][start.y]) {
			return null;
		}

		if (prevCellValue <= matrix[start.x][start.y]) {
			return null;
		}

		visitedCells[start.x][start.y] = true;

		// next max path
		int currMaxDepth = -1;
		List<Cell> resultPath = null;
		List<Cell> nextCells = new ArrayList<Cell>();
		nextCells.add(new Cell(start.x - 1, start.y));
		nextCells.add(new Cell(start.x + 1, start.y));
		nextCells.add(new Cell(start.x, start.y - 1));
		nextCells.add(new Cell(start.x, start.y + 1));

		for (Cell next : nextCells) {
			if (isCellValid(next, matrix)
					&& matrix[start.x][start.y] > matrix[next.x][next.y]) {
				List<Cell> path = findLongestDepth(matrix,
						matrix[start.x][start.y], next, visitedCells);
				if (path != null && currMaxDepth < path.size()) {
					resultPath = path;
					currMaxDepth = path.size();
				}
			}
		}
		if (resultPath == null) {
			resultPath = new ArrayList<Cell>();
		}
		resultPath.add(0, start);

		visitedCells[start.x][start.y] = false;

		return resultPath;
	}

	private static boolean isCellValid(Cell cell, int[][] matrix) {
		if (cell.x < 0 || cell.x > matrix.length - 1 || cell.y < 0
				|| cell.y > matrix.length - 1) {
			return false;
		}
		return true;
	}
}

class Cell {
	Cell(int xx, int yy) {
		this.x = xx;
		this.y = yy;
	}

	int x;
	int y;

	@Override
	public String toString() {
		return "(" + this.x + ", " + this.y + ")";
	}
}