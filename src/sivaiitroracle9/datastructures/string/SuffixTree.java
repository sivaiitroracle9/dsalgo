package sivaiitroracle9.datastructures.string;

import java.util.ArrayList;
import java.util.List;

public class SuffixTree {

	public static void main(String[] args) {
		String text = "a8abc1abc";
		SuffixTree t = new SuffixTree();
		t.constructSuffixTree(text);
	}

	public SuffixTreeNode constructSuffixTree(String text) {

		SuffixTreeNode root = new SuffixTreeNode();

		char[] textArray = text.toCharArray();
		int pos = textArray.length - 1;
		while (pos != -1) {
			push(root, pos, pos, text);
			pos--;
		}

		return root;
	}

	private void push(SuffixTreeNode node, int index, int pos, String text) {
		char ch = text.charAt(index);
		int i = 0;
		int len = node.nodeData != null ? node.nodeData.length() : 0;
		while (len != 0 && text.charAt(index) == node.nodeData.charAt(i)) {
			index++;
			i++;
		}

		if (len != 0 && node.nodeData.charAt(len - 1) == '$') {

			if (i == len - 1) {
				String t1 = node.nodeData.substring(0, i);
				String t2 = "$"; // root.nodeData.substring(i);

				SuffixTreeNode endNode = new SuffixTreeNode();
				endNode.nodeData = "$";
				endNode.index = node.index;
				node.childNodes.add(endNode);

				SuffixTreeNode branchNode = new SuffixTreeNode();
				branchNode.nodeData = text.substring(index) + '$';
				branchNode.index = pos;
				node.childNodes.add(branchNode);

				node.nodeData = t1;
				node.index = -1;
			} else if (i < len - 1) {
				String t1 = node.nodeData.substring(0, i);
				String t2 = node.nodeData.substring(i);

				SuffixTreeNode endNode = new SuffixTreeNode();
				endNode.nodeData = t2;
				endNode.index = node.index;
				node.childNodes.add(endNode);

				SuffixTreeNode branchNode = new SuffixTreeNode();
				branchNode.nodeData = text.substring(index) + '$';
				branchNode.index = pos;
				node.childNodes.add(branchNode);

				node.nodeData = t1;
				node.index = -1;
			}

		} else {

			if (i < len - 1) {

				SuffixTreeNode endNode = new SuffixTreeNode();
				endNode.childNodes.addAll(node.childNodes);
				endNode.nodeData = node.nodeData.substring(i);

				SuffixTreeNode branchNode = new SuffixTreeNode();
				branchNode.nodeData = text.substring(index) + "$";
				branchNode.index = pos;

				node.childNodes.clear();
				node.childNodes.add(branchNode);
				node.childNodes.add(endNode);
				node.index = -1;
				node.nodeData = node.nodeData.substring(0, i);
			} else {

				List<SuffixTreeNode> childList = node.childNodes;
				SuffixTreeNode childNode = null;

				for (SuffixTreeNode child : childList) {
					if (child.nodeData.charAt(0) == text.charAt(index)) {
						childNode = child;
						break;
					}
				}

				if (childNode != null) {
					push(childNode, index, pos, text);
				} else {
					SuffixTreeNode branchNode = new SuffixTreeNode();
					branchNode.nodeData = text.substring(index) + '$';
					branchNode.index = pos;
					node.childNodes.add(branchNode);
				}

			}

		}

	}

}

class SuffixTreeNode {
	String nodeData = null;
	int index = -1;
	List<SuffixTreeNode> childNodes = new ArrayList<SuffixTreeNode>();

}