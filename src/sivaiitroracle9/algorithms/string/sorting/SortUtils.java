package sivaiitroracle9.algorithms.string.sorting;

public class SortUtils {

	public static int[] swap(int[] array, int index1, int index2) {
		int temp = array[index2];
		array[index2] = array[index1];
		array[index1] = temp;
		return array;
	}

	public static void printArray(int[] array) {
		String line = "";
		for (int i = 0; i < array.length; i++) {
			line += "-" + array[i];
		}
		System.out.println(line);
		System.out.println();
	}

	public static int[] randomArray(int size) {

		int maxNum = (int) (Math.random() * 1000);

		int[] array = new int[size];
		for (int i = 0; i < size; i++) {
			array[i] = (int) (Math.random() * maxNum);
		}
		return array;
	}

}
