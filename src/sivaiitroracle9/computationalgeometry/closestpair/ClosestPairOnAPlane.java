package sivaiitroracle9.computationalgeometry.closestpair;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;

import sivaiitroracle9.computationalgeometry.utils.Point;

public class ClosestPairOnAPlane {

	List<Point> listOfPointsOnAPlane = null;
	TreeMap<Double, ArrayList<Point>> xPointList = null;
	Set<Point> pair = new HashSet<Point>();
	
	public static void main(String[] args){
		
		ClosestPairOnAPlane obj = new ClosestPairOnAPlane();
		
		List<Point> pointList = obj.placePointsOnPlane(20, 50);
		obj.printOnALine(pointList, 0, pointList.size()-1);
		Collections.sort(pointList);
		obj.printOnALine(pointList, 0, pointList.size()-1);
		int part = obj.partition(pointList, 0, pointList.size()-1);
		obj.printOnALine(pointList, 0, part);
		obj.printOnALine(pointList, part+1, pointList.size()-1);
		
	}

	private double findClosestPair(List<Point> listPoints, int left, int right) {
		
		if (left >= right) {
			return 0;
		}
		
		if (left + 1 == right) {
			pair.clear();
			pair.add(listPoints.get(left));
			pair.add(listPoints.get(right));
			return listPoints.get(left).distance(listPoints.get(right));
		}
		
		int mid = partition(listPoints, left, right);
		double leftDelta = findClosestPair(listPoints, left, mid);
		double rightDelta = findClosestPair(listPoints, mid+1, right);
		
		double delta = Math.min(leftDelta, rightDelta);
		
		if (delta==leftDelta) {
			
		}
	}

	private int partition(List<Point> listPoints, int left, int right) {
		
		xPointList = new TreeMap<Double, ArrayList<Point>>();
		for (int i = left; i <= right; i++) {
			if (xPointList.get(listPoints.get(i).X()) == null) {
				xPointList.put(listPoints.get(i).X(), new ArrayList<Point>());
			}
			xPointList.get(listPoints.get(i).X()).add(listPoints.get(i));
		}
		
		int k=0;
		while(true) {
			double key = xPointList.firstKey();
			int size = xPointList.get(key).size();
			if (k+size < (right - left)/2) {
				k +=size;
				xPointList.remove(key);
			} else {
				return k;
			}
		}
	}

	public List<Point> placePointsOnPlane(int numberOfPoints, int maxCoordinates) {
		listOfPointsOnAPlane = new ArrayList<Point>();

		for (int i = 0; i < numberOfPoints; i++) {
			double x = (int) (Math.random() * maxCoordinates);
			double y = (int) (Math.random() * maxCoordinates);
			listOfPointsOnAPlane.add(i, new Point(x, y));
		}
		return listOfPointsOnAPlane;
	}
	
	public void printOnALine (List<Point> pointList, int startIndex, int endIndex) {
		String sr = "";
		for (int i=startIndex; i<=endIndex; i++) {
			sr += pointList.get(i).toString();
		}
		System.out.println(sr);
	}

}
