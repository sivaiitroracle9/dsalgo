package sivaiitroracle9.algorithms.graphs;

import java.util.ArrayList;
import java.util.List;

public class LongestPathInMaze {

	private static List<Cell> findLongestPath(boolean[][] maze, Cell start,
			Cell end, boolean[][] visitedCells) {

		if (!isCellValid(start, maze) || !isCellValid(end, maze)) {
			return null;
		}

		if (visitedCells[start.x][start.y]) {
			return null;
		} else {
			// Update visited cells.
			visitedCells[start.x][start.y] = true;
		}

		if (start.x == end.x && start.y == end.y) {
			List<Cell> path = new ArrayList<Cell>();
			path.add(start);
			return path;
		}

		// next max path
		int currmax = -1;
		List<Cell> resultPath = null;
		List<Cell> nextCells = new ArrayList<Cell>();
		nextCells.add(new Cell(start.x - 1, start.y));
		nextCells.add(new Cell(start.x + 1, start.y));
		nextCells.add(new Cell(start.x, start.y - 1));
		nextCells.add(new Cell(start.x, start.y + 1));

		for (Cell next : nextCells) {
			List<Cell> path = findLongestPath(maze, next, end, visitedCells);
			if (path != null && currmax < path.size()) {
				resultPath = path;
				currmax = path.size();
				resultPath.add(0, start);
			}
		}

		visitedCells[start.x][start.y] = false;
		return resultPath;

	}

	private static boolean isCellValid(Cell cell, boolean[][] maze) {
		if (cell.x < 0 || cell.x > maze.length - 1 || cell.y < 0
				|| cell.y > maze.length - 1) {
			return false;
		}

		if (maze[cell.x][cell.y]) {
			return false; // blocked cell.
		}

		return true;
	}

	private static boolean[][] createRandomMaze(int size) {

		if (size <= 0)
			return null;

		boolean[][] maze = new boolean[size][size];
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				maze[i][j] = (int) (Math.random() * 3) == 1;
			}
		}
		maze[0][0] = false;
		maze[size - 1][size - 1] = false;
		return maze;
	}

	private static void printMaze(boolean[][] maze) {

		for (int i = 0; i < maze.length; i++) {
			String line = "";
			for (int j = 0; j < maze[0].length; j++) {
				if (maze[i][j]) {
					line += "#|";
				} else {
					line += "_|";
				}
			}
			System.out.println(line);
		}
	}

	private static void printSolvedMaze(boolean[][] maze, List<Cell> path) {

		boolean[][] finalMaze = new boolean[maze.length][maze[0].length];
		for (Cell cell : path) {
			finalMaze[cell.x][cell.y] = true;
		}

		for (int i = 0; i < maze.length; i++) {
			String line = "";
			for (int j = 0; j < maze[0].length; j++) {
				if (maze[i][j]) {
					line += "#|";
				} else {
					if (finalMaze[i][j]) {
						line += "*|";
					} else {
						line += "_|";
					}
				}
			}
			System.out.println(line);
		}
	}

	public static void main(String[] args) {
		int size = 5;
		boolean[][] maze = createRandomMaze(size);
		printMaze(maze);
		System.out.println();

		List<Cell> path = findLongestPath(maze, new Cell(0, 0), new Cell(
				size - 1, size - 1), new boolean[size][size]);

		if (path == null) {
			System.out.println("No Path");
		} else {
			String line = "";
			for (Cell c : path) {
				line += c.toString() + "->";
			}
			System.out.println(line);
			System.out.println();

			printSolvedMaze(maze, path);
		}
	}

}