package sivaiitroracle9.algorithms.string.sorting;

public class MergeSort {

	public static void main(String[] args) {

		int[] array = SortUtils.randomArray(10);
		SortUtils.printArray(array);
		array = mergeSort(array, 0, array.length-1);
	}

	private static int[] mergeSort(int[] array, int left, int right) {
		
		if (left>=right) {
			int[] a = new int[1];
			a[0]=array[left];
			return a;
		}

		int mid = (left + right) / 2;

		int[] leftArray = mergeSort(array, left, mid - 1);
		int[] rightArray = mergeSort(array, mid, right);

		int[] sortedArray = new int[leftArray.length + rightArray.length];

		int sleft = 0;
		int sright = 0;

		while (sleft < leftArray.length) {
			while (sleft < rightArray.length) {
				if (leftArray[sleft] < rightArray[sright]) {
					sortedArray[sleft + sright] = leftArray[sleft];
					sleft++;
				} else {
					sortedArray[sleft + sright] = rightArray[sright];
					sright++;
				}
				break;
			}
		}

		return sortedArray;
	}

}
