package sivaiitroracle9.algorithms.graphs;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.NavigableMap;
import java.util.Scanner;
import java.util.TreeMap;

public class LongestSkiing {

	private int m;
	private int n;
	private int[][] mapMatrix = null;
	private Path[][] longestPathMatrix = null;

	public static void main(String[] args) {
		LongestSkiing obj = new LongestSkiing();
		obj.readData(args[0]);
		obj.findLongestPath();
	}

	private class Path implements Comparable<Path> {
		private ArrayList<Node> path;
		private int pathSize;
		private int totalDrop;

		Path() {
			path = new ArrayList<Node>();
		}

		@Override
		public int compareTo(Path that) {
			if (this.pathSize == that.pathSize) {
				return this.totalDrop - that.totalDrop;
			}
			return this.pathSize - that.pathSize;
		}

		public void addPath(Node node) {
			path.add(node);
			pathSize = path.size();
			totalDrop = this.path.get(this.path.size() - 1).value
					- this.path.get(0).value;
		}

		public void printPath() {
			for (int i = path.size() - 1; i > 0; i--) {
				Node node = path.get(i);
				System.out.print("{" + node.x + ", " + node.y + ", "
						+ node.value + "}" + " -> ");
			}
			System.out.print("{" + path.get(0).x + ", " + path.get(0).y + ", "
					+ path.get(0).value + "}");
			System.out.println();
			System.out.println();
			Node start = path.get(path.size() - 1);
			Node end = path.get(0);
			System.out.println("Start Point = ( " + start.x + ", " + start.y
					+ ") ... Elevation = " + start.value);
			System.out.println("End Point = ( " + end.x + ", " + end.y
					+ ") ... Elevation = " + end.value);
			System.out.println("Path length = " + pathSize);
			System.out.println("Drop = " + totalDrop);
		}

	}

	private class Node {
		private int value;
		private int x;
		private int y;

		Node(int x, int y, int value) {
			this.x = x;
			this.y = y;
			this.value = value;
		}
	}

	public void findLongestPath() {

		TreeMap<Integer, List<Path>> tmap = new TreeMap<Integer, List<Path>>();
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				if (!isAlreadyLengthFound(i, j)) {
					// if Path is already computed it cannot be the local maxima
					// in a region
					// or cannot be the maximum of a path.
					Path path = skii(i, j);

					if (tmap.get(path.pathSize) == null) {
						tmap.put(path.pathSize, new ArrayList<Path>());
					}
					tmap.get(path.pathSize).add(path);
				}
			}
		}

		NavigableMap<Integer, List<Path>> dTmap = tmap.descendingMap();
		List<Path> finalLongestPaths = dTmap.firstEntry().getValue();
		if (finalLongestPaths.size() != 0) {
			Collections.sort(finalLongestPaths, Collections.reverseOrder());
			finalLongestPaths.get(0).printPath();
		} else {
			System.out.println("No Paths Found.");
		}

	}

	private Path skii(int i, int j) {

		if (isAlreadyLengthFound(i, j)) {
			return longestPathMatrix[i][j];
		}

		Node curNode = new Node(i, j, mapMatrix[i][j]);
		List<Path> listOfPaths = new ArrayList<LongestSkiing.Path>();

		int cnt = 0;
		if (i < m - 1 && mapMatrix[i][j] > mapMatrix[i + 1][j]) {

			listOfPaths.add(cnt, copyToPath(skii(i + 1, j)));
			listOfPaths.get(cnt).addPath(curNode);
			cnt++;
		}

		if (i > 0 && mapMatrix[i][j] > mapMatrix[i - 1][j]) {
			listOfPaths.add(cnt, copyToPath(skii(i - 1, j)));
			listOfPaths.get(cnt).addPath(curNode);
			cnt++;
		}

		if (j < n - 1 && mapMatrix[i][j] > mapMatrix[i][j + 1]) {
			listOfPaths.add(cnt, copyToPath(skii(i, j + 1)));
			listOfPaths.get(cnt).addPath(curNode);
			cnt++;
		}

		if (j > 0 && mapMatrix[i][j] > mapMatrix[i][j - 1]) {
			listOfPaths.add(cnt, copyToPath(skii(i, j - 1)));
			listOfPaths.get(cnt).addPath(curNode);
			cnt++;
		}

		if (listOfPaths.size() != 0) {
			Collections.sort(listOfPaths, Collections.reverseOrder());
		} else {
			listOfPaths.add(new Path());
			listOfPaths.get(0).addPath(curNode);
		}

		longestPathMatrix[i][j] = listOfPaths.get(0);
		return listOfPaths.get(0);
	}

	private Path copyToPath(Path orij) {
		Path newPath = new Path();
		newPath.path.addAll(orij.path);
		newPath.pathSize = orij.pathSize;
		newPath.totalDrop = orij.totalDrop;
		return newPath;
	}

	private boolean isAlreadyLengthFound(int i, int j) {
		if (longestPathMatrix[i][j] != null) {
			return true;
		}
		return false;
	}

	public void readData(String filePath) {
		Scanner s = null;

		try {
			s = new Scanner(new File(filePath));
		} catch (FileNotFoundException e) {
			System.out.println("Error reading data !!");
			System.exit(-1);
		}

		m = s.nextInt();
		n = s.nextInt();

		mapMatrix = new int[m][n];
		longestPathMatrix = new Path[m][n];
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				mapMatrix[i][j] = s.nextInt();
				longestPathMatrix[i][j] = null;
			}
		}
	}
}
