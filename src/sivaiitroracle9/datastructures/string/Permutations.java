package sivaiitroracle9.datastructures.string;

import java.util.ArrayList;
import java.util.List;

public class Permutations {

	private static void permutations(int[] charArray, int k, int n,
			List<Integer> prefix, List<List<Integer>> permutations) {

		if (k < 0 || k > n || permutations == null) {
			return;
		}

		if (k == n) {

			if (prefix == null || (prefix != null && prefix.isEmpty())) {
				return;
			}

			ArrayList<Integer> perm = new ArrayList<Integer>();
			perm.addAll(0, prefix);
			permutations.add(perm);
		} else {
			int charK = charArray[k];
			for (int i = k; i < n; i++) {

				prefix.add(k, charArray[i]);
				// Swap 0 with i
				charArray[k] = charArray[i];
				charArray[i] = charK;
		
				permutations(charArray, k + 1, n, prefix, permutations);
		
				// Swap i with 0
				charArray[i] = charArray[k];
				charArray[k] = charK;
				prefix.remove(k);
			}
		}
		return;
	}

	public static List<List<Integer>> permuatations(int[] charArray) {
		List<List<Integer>> permutations = new ArrayList<List<Integer>>();

		permutations(charArray, 0, charArray.length, new ArrayList<Integer>(),
				permutations);

		return permutations;

	}

	public static void main(String[] args) {

		int[] array = new int[4];
		array[0] = 1;
		array[1] = 2;
		array[2] = 3;
		array[3] = 4;
		for (List<Integer> perm : permuatations(array)) {
			System.out.println(perm);
		}

	}

}
