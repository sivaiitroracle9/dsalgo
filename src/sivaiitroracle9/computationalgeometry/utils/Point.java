package sivaiitroracle9.computationalgeometry.utils;

public class Point implements Comparable<Point>{

	private double x;
	private double y;

	public Point(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public double X() {
		return this.x;
	}

	public void X(double x) {
		this.x = x;
	}

	public double Y() {
		return this.y;
	}

	public void Y(double y) {
		this.y = y;
	}

	public boolean equals(Point that) {
		if (this.x == that.X() && this.y == that.Y()) {
			return true;
		}
		return false;
	}

	@Override
	public int compareTo(Point that){
		if (this.x != that.x) {
			return (this.x-that.x) > 0 ? 1: (this.x-that.x) == 0 ? 0 : -1;
		} else {
			return (this.y-that.y) > 0 ? 1: (this.y-that.y) == 0 ? 0 : -1;
		}
	}
	
	public Point differnce(Point point) {
		return new Point(this.x - point.X(), this.y - point.Y());
	}

	public double distance(Point point) {

		return Math.sqrt(Math.pow(this.x - point.X(), 2)
				+ Math.pow(this.y - point.Y(), 2));
	}

	public double polarAngle(Point p) {
		return Math.atan2(p.Y() - this.y, p.X() - this.x);
	}
	
	public String toString(){
		return "-(" + this.x + ", " + this.y + ")-";
	}
}
