package jp.co.worksap.global;

public class Coordinate {
	private int x;
	private int y;

	public Coordinate(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return this.x;
	}

	public int getY() {
		return this.y;
	}

	public Coordinate getRight() {

		return new Coordinate(this.x + 1, y);
	}

	public Coordinate getLeft() {

		return new Coordinate(this.x - 1, y);
	}

	public Coordinate getUp() {

		return new Coordinate(this.x, y + 1);
	}

	public Coordinate getDown() {

		return new Coordinate(this.x, y - 1);
	}

	@Override
	public String toString() {
		return "(" + this.x + "," + this.y + ")";
	}

	@Override
	public boolean equals(Object obj) {

		if (obj != null && obj instanceof Coordinate) {
			return (this.x == ((Coordinate) obj).getX())
					&& (this.y == ((Coordinate) obj).getY());

		} else {
			return false;
		}

	}
}
