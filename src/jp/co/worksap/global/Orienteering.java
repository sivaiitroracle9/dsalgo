package jp.co.worksap.global;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Orienteering {

	private int INPUT_MIN_WIDTH = 1;
	private int INPUT_MIN_HEIGHT = 1;
	private int INPUT_MAX_WIDTH = 100;
	private int INPUT_MAX_HEIGHT = 100;
	private int INPUT_MAX_CHECKPOINTS = 18;

	private Coordinate START;
	private Coordinate GOAL;
	private int INFINITE = Integer.MAX_VALUE;
	private int xmin, xmax, ymin, ymax;

	private List<Coordinate> checkpoints = new ArrayList<Coordinate>();
	private List<Coordinate> closedblocks = new ArrayList<Coordinate>();

	private Map<String, Integer> weightGraphMap = new HashMap<String, Integer>();

	private Map<String, Integer> minDistanceMap = new HashMap<String, Integer>();

	public Orienteering(Coordinate startPoint, Coordinate goalPoint) {
		this.START = startPoint;
		this.GOAL = goalPoint;
	}

	public void setBoundaries(int width, int height) {
		this.xmin = 0;
		this.ymin = 0;
		this.xmax = width - 1;
		this.ymax = height - 1;
	}

	public void setCheckPoints(List<Coordinate> checkpoints) {
		(this.checkpoints).addAll(checkpoints);
	}

	public void setClosedBlocks(List<Coordinate> closedblocks) {
		(this.closedblocks).addAll(closedblocks);
	}

	private boolean validateInput(int width, int height, int checkPoints) {

		return (width >= INPUT_MIN_WIDTH) && (width <= INPUT_MAX_WIDTH)
				&& (height >= INPUT_MIN_HEIGHT) && (height <= INPUT_MAX_HEIGHT)
				&& (checkPoints <= INPUT_MAX_CHECKPOINTS);
	}

	public static void main(String[] args) {

		Coordinate START = null;
		Coordinate GOAL = null;
		List<Coordinate> openblocks = new ArrayList<Coordinate>();
		List<Coordinate> checkpoints = new ArrayList<Coordinate>();
		List<Coordinate> closedblocks = new ArrayList<Coordinate>();
		int width = 0, height = 0;

		// Read Input
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		try {
			String text = br.readLine();
			int count = 0;
			String[] dimensions = text.split(" ");
			if (dimensions.length == 2) {
				width = Integer.parseInt(dimensions[0].trim());
				height = Integer.parseInt(dimensions[1].trim());
			} else {
				System.out.println(-1);
				System.exit(1);
			}

			while (text != null && !text.equals("")) {
				count++;
				text = br.readLine();
				String line = text.trim();
				char[] charArr = line.toCharArray();
				for (int i = 0; i < width; i++) {
					Coordinate co = new Coordinate(i, count - 1);
					if (charArr[i] == '@') {
						checkpoints.add(co);
					} else if (charArr[i] == '#') {
						closedblocks.add(co);
					} else if (charArr[i] == 'S') {
						START = new Coordinate(i, count - 1);
					} else if (charArr[i] == 'G') {
						GOAL = new Coordinate(i, count - 1);
					}
				}
				
				if (count == height)
					break;
			}

		} catch (IOException ioe) {
			System.out.println(-1);
			System.exit(1);
		}

		// Game Creation and Start
		Orienteering game = new Orienteering(START, GOAL);
		game.setBoundaries(width, height);
		game.setCheckPoints(checkpoints);
		game.setClosedBlocks(closedblocks);
		int dist = game.shortestDistance();
		System.out.println(dist);
		System.exit(1);
	}

}
