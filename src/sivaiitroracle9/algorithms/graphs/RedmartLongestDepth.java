package sivaiitroracle9.algorithms.graphs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

public class RedmartLongestDepth {

	public static void main(String[] args) {
		int size = 5;
		int maxNum = 15;
		int[][] matrix = createRandomMatrix(size, maxNum);
		printMatrix(matrix, maxNum);

		TreeMap<Integer, Map<Cell, List<Cell>>> depthMap = findLongestDepth(matrix);
		NavigableMap<Integer, Map<Cell, List<Cell>>> reverseDepthMap = depthMap
				.descendingMap();

		for (Cell cell : (depthMap.get(reverseDepthMap.firstKey())).keySet()) {
			List<Cell> path = (depthMap.get(reverseDepthMap.firstKey()))
					.get(cell);
			if (path == null) {
				System.out.println("No Path");
			} else {
				String line = "";
				int dp = 0;
				for (Cell c : path) {
					line += c.toString() + "->";
					dp = matrix[c.x][c.y];
				}
				System.out.println(line + (matrix[cell.x][cell.y]-dp));
				System.out.println();
			}
		}

	}

	private static TreeMap<Integer, Map<Cell, List<Cell>>> findLongestDepth(
			int[][] matrix) {

		TreeMap<Integer, Map<Cell, List<Cell>>> depthMap = new TreeMap<Integer, Map<Cell, List<Cell>>>();

		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {
				Cell curCell = new Cell(i, j);

				List<Cell> maxDepthPath = findLongestDepth(matrix,
						Integer.MAX_VALUE, curCell,
						new boolean[matrix.length][matrix[0].length]);
				if (maxDepthPath != null) {
					int depth = maxDepthPath.size();
					if (depthMap.get(depth) == null) {
						depthMap.put(depth, new HashMap<Cell, List<Cell>>());
					}

					depthMap.get(depth).put(curCell, maxDepthPath);

				}
			}
		}

		return depthMap;
	}

	private static List<Cell> findLongestDepth(int[][] matrix,
			int prevCellValue, Cell start, boolean[][] visitedCells) {

		if (!isCellValid(start, matrix) || visitedCells[start.x][start.y]) {
			return null;
		}

		if (prevCellValue <= matrix[start.x][start.y]) {
			return null;
		}

		visitedCells[start.x][start.y] = true;

		// next max path
		int currMaxDepth = -1;
		List<Cell> resultPath = null;
		List<Cell> nextCells = new ArrayList<Cell>();
		nextCells.add(new Cell(start.x - 1, start.y));
		nextCells.add(new Cell(start.x + 1, start.y));
		nextCells.add(new Cell(start.x, start.y - 1));
		nextCells.add(new Cell(start.x, start.y + 1));

		for (Cell next : nextCells) {
			if (isCellValid(next, matrix)
					&& matrix[start.x][start.y] > matrix[next.x][next.y]) {
				List<Cell> path = findLongestDepth(matrix,
						matrix[start.x][start.y], next, visitedCells);
				if (path != null && currMaxDepth < path.size()) {
					resultPath = path;
					currMaxDepth = path.size();
				}
			}
		}
		if (resultPath == null) {
			resultPath = new ArrayList<Cell>();
		}
		resultPath.add(0, start);

		visitedCells[start.x][start.y] = false;

		return resultPath;
	}

	private static boolean isCellValid(Cell cell, int[][] matrix) {
		if (cell.x < 0 || cell.x > matrix.length - 1 || cell.y < 0
				|| cell.y > matrix.length - 1) {
			return false;
		}
		return true;
	}

	private static void printMatrix(int[][] matrix, int maxNum) {
		for (int i = 0; i < matrix.length; i++) {
			String len = "";
			for (int j = 0; j < matrix[0].length; j++) {
				String sr = "";
				int init = (int) Math.log10(matrix[i][j]) + 1;
				int max = (int) Math.log10(maxNum) + 1;
				for (int m = init;  m <max; m++) {
					sr += "0";
				}
				sr += matrix[i][j];

				len += sr + "|";
			}
			System.out.println(len);
		}
		System.out.println("-----------------------------------------------");
	}

	private static int[][] createRandomMatrix(int size, int maxNum) {
		if (size <= 0) {
			return null;
		}

		int[][] matrix = new int[size][size];
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				matrix[i][j] = (int) (Math.random() * maxNum);
			}
		}
		return matrix;
	}
}
